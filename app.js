/* global Vue axios */ //> from vue.html
const $ = sel => document.querySelector(sel)

const incidents = new Vue ({
    el:'#app',
    data() {
        return {
            list: [],
            isLoading: false
        }
    },
    methods: {
        search: ({target:{value:v}}) => incidents.fetch (v && '$search='+v),
        fetch (_filter='') {
            incidents.isLoading = true;
            //const columns = 'id,short_description,caller_id,category,subcategory,cmdb_ci,assignment_group'
            //const response = await axios.get(`https://6085f360d14a87001757881b.mockapi.io/api/v1/incidents?$select=${columns}&${_filter}`)
            const response = axios.get(`https://9geg6jzse4.execute-api.us-east-1.amazonaws.com/default/MM_Team_Classify_Test`).then((response) => {
                incidents.isLoading = false;
                incidents.list = response.data
            })
        },
        classifyIncidents: () => {
            incidents.isLoading = true;
            axios.post('https://9geg6jzse4.execute-api.us-east-1.amazonaws.com/default/MM_Team_Classify_Test').then((response) => {
                incidents.list = response.data.map((incident) => {
                    incidents.isLoading = false;
                    incident.assignment_group = incident.classified_assignment_group.predictions[0].predicted_label
                    return incident;
                })
            })
        }
    }
})

// Get list of incidents on the app initialization
incidents.fetch()